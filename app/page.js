'use client'
import Image from 'next/image'
import { useTheme } from 'next-themes'
import { useState, useEffect } from 'react'
import Link from 'next/link'

export default function Home() {
  const [mounted, setMounted] = useState(false)
	const { theme, setTheme } = useTheme()

	useEffect(() => {
		setMounted(true)
	}, [])
	
	if (!mounted) {
		return null
	}

	return (
		<div className='min-h-screen w-screen bg-sky-50 dark:bg-slate-800'>
			<div 
			className='absolute top-3 right-5 p-2 border-solid border-2 border-stone-500 rounded-full hover:border-stone-700 hover:cursor-pointer hover:scale-105 dark:border-sky-50'
			onClick={() => {
				setTheme(theme === 'light' ? 'dark' : 'light')
			}}	
			>
				<Image
					src= {theme === 'light' ? '/icon/moon.svg' : '/icon/sun.svg'}
					alt="Moon/Sun Logo"
					className="dark:invert"
					width={25}
					height={24}
					priority
				/>
			</div>

			<div className='grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 sm:gap-y-8 justify-items-center pt-9 lg:px-28'>
				<Link href="/qr">
					<div className='p-2 rounded-md border-solid border-2 shadow-md hover:cursor-pointer hover:scale-105 hover:border-sky-200'>
						<div className='flex flex-col justify-center items-center divide-y divide-sky-200 gap-2'>
							<Image
								src="/icon/qr-code.svg"
								alt="QR Logo"
								className="dark:invert"
								width={150}
								height={24}
								priority
							/>
							<p className='w-full text-center font-semibold text-lg pt-2'>QR Generator</p>
						</div>
					</div>
				</Link>
		
				<Link href="https://quran.inx.my.id" target='_blank'>
					<div className='p-2 rounded-md border-solid border-2 shadow-md hover:cursor-pointer hover:scale-105 hover:border-sky-200'>
						<div className='flex flex-col justify-center items-center divide-y divide-sky-200 gap-2'>
							<Image
								src="/icon/quran.svg"
								alt="Quran Logo"
								className="dark:invert"
								width={150}
								height={24}
								priority
							/>
							<p className='w-full text-center font-semibold text-lg pt-2'>Quran</p>
						</div>
					</div>
				</Link>

				<Link href="https://s.inx.my.id" target='_blank'>
					<div className='p-2 rounded-md border-solid border-2 shadow-md hover:cursor-pointer hover:scale-105 hover:border-sky-200'>
						<div className='flex flex-col justify-center items-center divide-y divide-sky-200 gap-2'>
							<Image
								src="/icon/chain.svg"
								alt="Sinx Logo"
								className="dark:invert"
								width={150}
								height={24}
								priority
							/>
							<p className='w-full text-center font-semibold text-lg pt-2'>Shorten Link</p>
						</div>
					</div>
				</Link>

				<Link href="/movies">
					<div className='p-2 rounded-md border-solid border-2 shadow-md hover:cursor-pointer hover:scale-105 hover:border-sky-200'>
						<div className='flex flex-col justify-center items-center divide-y divide-sky-200 gap-2'>
							<Image
								src="/icon/movies.svg"
								alt="QR Logo"
								className="dark:invert"
								width={150}
								height={24}
								priority
							/>
							<p className='w-full text-center font-semibold text-lg pt-2'>Movies</p>
						</div>
					</div>
				</Link>
			</div>
		</div>
	)
}
